from django.db import models
import uuid

class UuidModel(models.Model):
    class Meta:
        abstract = True

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)

class Contact(UuidModel):
    pass
