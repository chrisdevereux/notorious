from rest_framework import viewsets
from notorious import models, serializers

class ContactViewSet(viewsets.ModelViewSet):
    queryset = models.Contact.objects.all()
    serializer_class = serializers.SnippetSerializer
