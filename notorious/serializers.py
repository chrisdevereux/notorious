from rest_framework import serializers
from notorious import models

class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contact
        fields = ('id', )
