import React, { useEffect, useState, FC } from 'react';
import Swiper from 'react-native-deck-swiper'
import { Button, Text, List, StyleService, useStyleSheet, ListItem, Toggle } from '@ui-kitten/components';
import * as Contacts from 'expo-contacts';
import { LayoutListElement } from '../../components/layout-list.component';
import { SafeAreaLayout } from '../../components/safe-area-layout.component';
import { useBackend } from '../../services/backend.service';
import { ScreenComponent } from '../../interfaces';
import { useLocalContacts, localContactsStore, SavedContact } from '../../services/local-contacts.service';
import { View, ListRenderItemInfo } from 'react-native';

const getContacts = async () => {
  const { status } = await Contacts.requestPermissionsAsync();

  if (status !== 'granted') {
    throw Error('Nah')
  }

  const { data } = await Contacts.getContactsAsync({
    fields: [Contacts.Fields.Emails, Contacts.Fields.Name, Contacts.Fields.PhoneNumbers],
  });
  
  return data
}

export const AddContacts: ScreenComponent = ({ navigation }) => {
  const localContacts = useLocalContacts()
  const styles = useStyleSheet(themedStyle);

  return (
    <SafeAreaLayout insets='top'>
    <View style={[styles.container, styles.centered]}>
        <Text category="h1">
          Contacts please yeah?
        </Text>

        <Button onPress={async () => {
          const contacts = await getContacts()
          localContacts.setContacts(contacts)
          navigation.navigate('RateContacts')
        }}>Ok</Button>
      </View>
    </SafeAreaLayout>
  );
};

export const RateContacts: ScreenComponent = ({ navigation }) => {
  const styles = useStyleSheet(themedStyle);

  const backend = useBackend()
  const store = useLocalContacts()

  return (
    <SafeAreaLayout insets='top'>
      <View style={styles.container}>
        <Text style={{ padding: 12, fontSize: 18, fontWeight: 'bold' }}>
          Select everyone who is a Labour member
        </Text>
        <List style={styles.component} data={store.contacts} renderItem={({ item }: ListRenderItemInfo<SavedContact>) => (
          <ListItem key={item.id} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>{item.payload.name}</Text>
            <Toggle checked={item.member} onChange={checked => store.setIsMember(item.id, checked)} />
          </ListItem>
        )}/>
        <View>
          <Button>Next</Button>
        </View>
      </View>
    </SafeAreaLayout>
  );
};

const themedStyle = StyleService.create({
  container: {
    backgroundColor: 'background-basic-color-2',
    height: '100%'
  },
  centered: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24
  },
  component: {
    flex: 1
    // ignore showcase container padding
  },
});
