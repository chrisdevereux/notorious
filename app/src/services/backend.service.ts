export const useBackend = () => new class Backend {
  async request(path: string, opts: RequestInit) {
    const res = await fetch('http://localhost:8000/api' + path, opts)
    if (!res.ok) {
      throw Error(await res.text())
    }
    
    return res.json()
  }

  post(path, { body, ...opts}) {
    return this.request(path, { method: 'POST', body: JSON.stringify(body), ...opts })
  }

  submitContacts(body: {}) {
    return this.post('/contacts', { body })
  }
}
