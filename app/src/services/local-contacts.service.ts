import { useState } from "react"
import { useStore } from "outstated"
import { Contact } from "expo-contacts"
import uuid from 'uuid'
import { fromPairs, sortBy } from 'lodash'

type Rating =
  | 'comrade'
  | 'waverer'
  | 'melt'

export type SavedContact = {
  id: string
  payload: Contact
  member: boolean
  rating?: Rating
}

export const localContactsStore = () => {
  const [contacts, setContacts] = useState<{ [key: string]: SavedContact }>({})
  return {
    contacts: sortBy(Object.values(contacts), c => c.payload.name + c.id),
    setContacts: (contacts: Contact[]) => {
      setContacts(
        fromPairs(
          contacts.map(payload => {
            const id = uuid();
            return [id, { id, payload, member: false }]
          })
        )
      )
    },
    setIsMember: (id: string, member: boolean) => {
      setContacts({
        ...contacts,
        [id]: {
          ...contacts[id],
          member
        }
      })
    }
  }
}

export const useLocalContacts = () => useStore(localContactsStore)
