import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { AddContacts, RateContacts } from '../scenes/onboarding/onboarding.scene'

export const Stack = createStackNavigator()

export const NotoriousNavigator = () => (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="AddContacts"  component={AddContacts} />
    <Stack.Screen name="RateContacts"  component={RateContacts} />
  </Stack.Navigator>
)
