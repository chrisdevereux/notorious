import { ComponentType } from "react";
import { NavigationHelpers } from "@react-navigation/core";

export type ScreenComponent = ComponentType<{navigation: NavigationHelpers<any> }>
