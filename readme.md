# Notorious

Relational organising prototype.

## Backend

Simple django app using django-rest-framework.

Start the database

```bash
docker-compose up
```

Start the backend:

```bash
pipenv install
pipenv shell
./manage.py migrate
./manage.py runserver
```

## Frontend

Expo react-native app using ui-kitten as design system and react-navigation for routing.

Forked from ui-kitten example project. Hit 'app' on the bottom tab bar to use the app. notorious.navigator is the UI root for this.

```bash
cd app
yarn (ios|android|web)
```
